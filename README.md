# incredible-master-server

## API DOCUMENTATION

### POST /api/login

#### Required parameters:
	* email
	* password
	
#### Expected Response:
	* AccessToken
	* error => x
	
### GET /api/servers

#### Expected response:
	* Server list of current online servers.
	
### GET /api/servers/all

#### Expected response:
	* Server list of all the servers.
	
### POST /api/servers

#### Required parameters:
	* port
	* num_conn
	* max_conn
	* name
	* description
	* status
	* next_map
	* map

#### Expected response:
	* success => The server has been added
	
#### Note:

If the server already exists, it will update the values to the server's values that were sent.




