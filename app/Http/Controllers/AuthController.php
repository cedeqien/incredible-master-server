<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email'     => 'required|string|email',
            'password'  => 'required'
        ]);

        $user = User::where('email', $request->email)->first();

        if($user)
        {
            if(Hash::check($request->password, $user->password)) {
                return response()->json(['accessToken' => $user->createToken('game')->accessToken]);
            } else {
                return response()->json(['error' => 'invalid password']);
            }
        } else {
            return response()->json(['error' => 'invalid user']);
        }
    }
}
