<?php

namespace App\Http\Controllers;

use App\Server;
use Illuminate\Http\Request;

class ServerController extends Controller
{
    public function index()
    {
        $servers = Server::where('online', '1')->get();
        return response()->json($servers, 200);
    }

    public function all_servers()
    {
        return response()->json(Server::all(), 200);
    }

    public function store(Request $request)
    {
        $server = Server::where([
            'ip', request()->ip(),
            'port', $request->port,
        ])->first();

        if($server)
        {
            $server->num_conn = $request->num_conn;
            $server->max_conn = $request->max_conn;
            $server->name = $request->name;
            $server->description = $request->description;
            $server->status = $request->status;
            $server->next_map = $request->next_map;
            $server->map = $request->map;
            $server->online = 1;
        } else {
            Server::create([
                'ip' => request()->ip(),
                'port' => $request->port,
                'num_conn' => $request->num_conn,
                'max_conn' => $request->max_conn,
                'name' => $request->name,
                'description' => $request->description,
                'status' => $request->status,
                'next_map' => $request->next_map,
                'map' => $request->map,
                'online' => 1
            ]);
        }

        return response()->json(['success'=>'The server has been added.']);
    }
}
