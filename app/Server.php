<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    const UPDATED_AT = 'updated';

    protected $fillable = [
        'ip', 'port', 'num_conn', 'max_conn', 'name', 'login', 'description', 'status',
        'next_map', 'map', 'official', 'online'
    ];

}
