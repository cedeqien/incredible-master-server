<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id');
            $table->ipAddress('ip');
            $table->integer('port');
            $table->integer('num_conn');
            $table->integer('max_conn');
            $table->string('name');
            $table->string('description');
            $table->string('status');
            $table->string('next_map');
            $table->string('map');
            $table->integer('official');
            $table->timestamp('updated');
            $table->boolean('online');
            $table->integer('user_id');

            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
